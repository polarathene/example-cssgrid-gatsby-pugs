# Pre-process original images script

Takes a list of images from the JSON data, along with some metadata to decide how to process images.

Reduces the size of images to what Gatsby needs to work with so that image processing will be faster. Applies some cropping and offset regions so all Gatsby needs to do is create size variants for `gatsby-image`.

As this is a once off process, the script is kept for reference and potential re-use, but it otherwise not a required part of this project.

## USAGE

- `yarn install` to download dependencies
- `node pre-process-images.js`, from the `preprocess` directory, else add relative path to file.

## NOTE

`input` directory and it's images are available from commit `de9454090a26ad61e91f321f5cd52a1609aeeff0`:
"chore: Add original JPG images for pre-processing with updated filenames"
