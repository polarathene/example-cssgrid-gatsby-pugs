/* eslint-disable no-console */
const path  = require("path")
const fs    = require("fs-extra")
const sharp = require("sharp")
const data  = require("../data/pugs.json")

// Target widths matching largest desired breakpoint(x3) to reduce processing time
const widths = {
  small_h: 1440,
  small_v: 1080,
  tall:    1080,
  big:     2160,
  wide:    2160,
}

// width:height
const aspectRatio = {
  small_h: [4, 3],
  small_v: [2, 3],
  tall:    [1, 2],
  big:     [1, 1.2],
  wide:    [3, 2],
}

function processImages(outputDir) {
  data.forEach(async pug => {
    // Modify the data JSON path to reference JPG input images instead
    const fpath = pug.localFile
    const fname = fpath.substr(fpath.lastIndexOf("/")).replace(/webp$/, "jpg")
    // __dirname, path relative from this file, not location 'node' is called from
    const filepath = path.join(__dirname, "input/", fname)
    const { name } = path.parse(filepath)

    const image = sharp(filepath)
    const { width: srcWidth, height: srcHeight } = await image.metadata()

    const targetWidth = widths[pug.image_type]
    const resizeParams = {
      fit: sharp.fit.cover,
      width: targetWidth,
      withoutEnlargement: true,
    }

    const [w, h] = aspectRatio[pug.image_type]
    const ar = h / w
    // Setting width and height for resize will crop by aspect ratio
    resizeParams.height = Math.round(targetWidth *  ar)

    const { crop } = pug
    if (crop) {
      // Automatically position crop region, instead of default center
      if (crop.mode === "autofocus") {
        resizeParams.position = sharp.strategy.attention
      }
      // Align crop region to edge/corner, accepts values from 'sharp.position'
      if (crop.mode === "align") {
        resizeParams.position = crop.position
      }

      // Crop region before resizing, width derived from aspect ratio
      // Shifts the cropped region left/right based on a 0-1 scale
      if (crop.mode === "focalpoint") {
        const {
          x: fpx = 0.5,
          y: fpy = 0.5,
          z: fpz = 1.0,
        } = crop.fp

        const cropRegion = {
          top:    0,
          left:   0,
          width:  srcWidth,
          height: srcHeight,
        }

        // Fits the aspectRatio region into the input image bounds
        const arFit = ar / (srcHeight / srcWidth)
        if (arFit > 1) {
          cropRegion.width = srcHeight / ar
        } else {
          cropRegion.height = srcWidth * ar
        }
        cropRegion.width =  Math.round(cropRegion.width  / fpz)
        cropRegion.height = Math.round(cropRegion.height / fpz)

        const x = Math.round((srcWidth - cropRegion.width) * fpx)
        cropRegion.left = Math.max(0, Math.min(x, srcWidth))

        const y = Math.round((srcHeight - cropRegion.height) * fpy)
        cropRegion.top = Math.max(0, Math.min(y, srcHeight))

        image.extract(cropRegion)
      }
    }

    const { effect } = pug
    if (effect === "grayscale") {
      image.grayscale()
    }

    console.log(`Preparing to process image: ${name}`)
    // 90% quality is still decent and keeps file size in git-lfs lower
    // NOTE: This does mean compound compression of 90% for the top breakpoint
    // eg 90% * 90% == 81%
    await image.resize(resizeParams)
      .webp({ quality: 90 })
      .toFile(`${outputDir}/${name}.webp`)
      .catch(err => {console.error(err)})

    const { queue: q, process: p } = sharp.counters()
    console.log(`Processed image: ${name} | Remaining: ${q + p}, Processing: ${p}`)
})
}

async function init() {
  const outputDir = path.join(__dirname, "output/")
  try {
    await fs.ensureDir(outputDir)
    processImages(outputDir)
  } catch (err) {
    console.error(err)
  }
}

init()

// Basic sharp usage example for other needs:
// sharp("relativeInput.webp").resize({ width:720 })
//   .webp({ nearLossless: true })
//   .toFile("relativeOutput.webp")
