import { graphql, useStaticQuery } from "gatsby"

const query = graphql`
fragment ImageData_tall on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(
          srcSetBreakpoints: [ 180, 360, 540, 720, 1080 ],
          toFormat: JPG,
          webpQuality: 75
        ) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_wide on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(
          srcSetBreakpoints: [ 360, 512, 720, 1080, 1600, 2160 ],
          toFormat: JPG,
          webpQuality: 75
        ) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_big on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(
          srcSetBreakpoints: [ 360, 512, 720, 1080, 1600, 2160 ],
          toFormat: JPG,
          webpQuality: 75
        ) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_small_h on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(
          srcSetBreakpoints: [ 240, 480, 720, 960, 1440 ],
          toFormat: JPG,
          webpQuality: 75
        ) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_small_v on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(
          srcSetBreakpoints: [ 180, 256, 360, 540, 800, 1080 ],
          toFormat: JPG,
          webpQuality: 75
        ) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}
query {
  tall: allPugsData(filter: {image_type: {eq: "tall"}}) {
    ...ImageData_tall
  }
  wide: allPugsData(filter: {image_type: {eq: "wide"}}) {
    ...ImageData_wide
  }
  big: allPugsData(filter: {image_type: {eq: "big"}}) {
    ...ImageData_big
  }
  small_h: allPugsData(filter: {image_type: {eq: "small_h"}}) {
    ...ImageData_small_h
  }
  small_v: allPugsData(filter: {image_type: {eq: "small_v"}}) {
    ...ImageData_small_v
  }
}
`

// useImageData
export default () => useStaticQuery(query)
