import React from "react"

import Layout from "../components/layout"
import ImageGrid from "../components/image-grid"
import useImageData from "../queries/image-data"

const Index = () => {
  const data = useImageData()

  return (
    <Layout>
      <ImageGrid imageData={data} hasBlur hasBG />
    </Layout>
  )
}

export default Index
